import pLimit from "p-limit";
import { map } from "./map";

export function mapLimit<T, K = any>(array: T[], fn: (item: T, index: number, arr: T[]) => Promise<K>, limitConcurrent: number) {
  const limit = pLimit(limitConcurrent);

  return map(array, (item, index, arr) => limit(() => fn(item, index, arr)));
}