import { delay } from './delay';
import { limitPool } from './limitPool';
import { map } from './map';
import { mapLimit } from './mapLimit';
import { nextFrame } from './nextFrame';
import { timeout } from './timeout';

export {
  delay,
  limitPool,
  map,
  mapLimit,
  nextFrame,
  timeout,
};

const promiseExtras = {
  delay,
  limitPool,
  map,
  mapLimit,
  nextFrame,
  timeout,
}

export default promiseExtras;
