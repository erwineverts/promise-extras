export function timeout<T>(targetPromise: Promise<T>, timeout: number) {
  let timeoutHandle: NodeJS.Timeout;
  const timeoutLimitPromise = new Promise((res, rej) => {
    timeoutHandle = setTimeout(
      () => rej(new Error('Timeout exceeded')),
      timeout
    );
  });
  return Promise.race([targetPromise, timeoutLimitPromise])
    .then((res) => {
      clearTimeout(timeoutHandle);
      return res as T;
    });
}