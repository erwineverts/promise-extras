export function nextFrame() {
  const nextTick = requestAnimationFrame || setImmediate;
  return new Promise(res => nextTick(res))
}