export async function map<T, K = any>(array: T[], fn: (item: T, index: number, arr: T[]) => Promise<K>) {
  return await Promise.all(array.map(fn))
}