import pLimit from 'p-limit'

export async function limitPool<T>(promiseArray: (() => Promise<T>)[], limitConcurrent: number) {
  const limit = pLimit(limitConcurrent);

  const input = promiseArray.map(p => limit(p));
  const result = await Promise.all(input);

  return result;
}